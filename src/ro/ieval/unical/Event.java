package ro.ieval.unical;

import java.util.ArrayList;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;

final class Event implements Parcelable{
	public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
		@Override
		public Event createFromParcel(final Parcel source) {
			return new Event(source.readLong(), source.readString(), source.readString(), source.readString(), source.readString(), source.readInt(), source.readLong(), source.readLong(), source.readLong());
		}

		@Override
		public Event[] newArray(final int size) {
			return new Event[size];
		}
	};

	private static final String[] PROJECTION = {
		BaseColumns._ID,
		Events.ORGANIZER,
		Events.TITLE,
		Events.EVENT_LOCATION,
		Events.DESCRIPTION,
		Events.EVENT_COLOR,
		Events.DTSTART,
		Events.DTEND,
		Events.CALENDAR_ID,
	};

	/** Event ID */
	public  long _id;
	/** Event owner */
	public String organizer;
	/** Event title */
	public String title;
	/** Event location */
	public String eventLocation;
	/** Event description */
	public String description;
	/** Event colour */
	public int eventColour;
	/** Event start time (UTC milliseconds since epoch) */
	public long dtstart;
	/** Event end time (UTC milliseconds since epoch) */
	public long dtend;
	/** Calendar ID */
	public long calendarID;

    public static Boolean calendarCreated=false;

    public Event() {
		dtstart = System.currentTimeMillis();
		dtend = dtstart + 60*60*1000;
	}

	public Event(final long _id, final String organizer, final String title, final String eventLocation, final String description, final int eventColor, final long dtstart, final long dtend, final long calendarID) {
		this._id=_id;
		this.organizer=organizer;
		this.title=title;
		this.eventLocation=eventLocation;
		this.description=description;
		this.eventColour=eventColor;
		this.dtstart=dtstart;
		this.dtend=dtend;
		this.calendarID=calendarID;
	}

	public static Event[] getEventsByCalendar(final Context context, final Calendar calendar, final String sort){
		final ContentResolver cr=context.getContentResolver();
		final Cursor cursor = cr.query(Events.CONTENT_URI,
				PROJECTION,
				Events.CALENDAR_ID+" = ?",
				new String[]{Long.toString(calendar._id)},
				sort);
		cursor.moveToFirst();
		final Event[] events = new Event[cursor.getCount()];
		for(int i=0;i<events.length;i++){
			int colour=cursor.getInt(5);
			colour=colour == 0 ? calendar.colour : colour;
			events[i]=new Event(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), colour, cursor.getLong(6), cursor.getLong(7), cursor.getLong(8));
			cursor.moveToNext();
		}
		cursor.close();
		return events;
	}

	public static Event getEventById(final Context context, final long id){
		final ContentResolver cr=context.getContentResolver();
		final Cursor cursor = cr.query(Events.CONTENT_URI,
									   PROJECTION,
									   BaseColumns._ID+" = ?",
									   new String[]{Long.toString(id)},
									   null);
		final Event event;
		if(cursor.moveToFirst()){
			int colour=cursor.getInt(5);
			colour=colour == 0 ? Calendar.getCalendarById(context, cursor.getLong(8)).colour : colour;
			event = new Event(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), colour, cursor.getLong(6), cursor.getLong(7), cursor.getLong(8));
		} else
			event = null;
		cursor.close();
		return event;
	}

	public static Event[] getAllEvents(final Context context) {
		final ArrayList<Event> r = new ArrayList<Event>();
		for(final Calendar calendar : Calendar.getAllCalendars(context))   {
            if(calendar.name.equals("unical")) calendarCreated=true;
			for(final Event event : getEventsByCalendar(context, calendar, null))
				r.add(event);
        }
		return  r.toArray(new Event[r.size()]);
	}

    public static long getUnicalId(final Context context) {
        for(final Calendar calendar : Calendar.getAllCalendars(context))
            if(calendar.name.equals("unical")) return calendar._id;

        return 0;
    }


    public void insert(final Context context) {
       ContentValues cv = new ContentValues();
        cv.put("calendar_id",calendarID);
        cv.put("title",title);
        cv.put("description",description);
        cv.put("dtstart",dtstart);
        cv.put("dtend",dtend);
        cv.put("eventTimezone", TimeZone.getDefault().getID());

        Uri url = context.getContentResolver().insert(Events.CONTENT_URI, cv);
    }
    public void edit(final Context context) {
        ContentValues cv = new ContentValues();
        cv.put("calendar_id",calendarID);
        cv.put("title",title);
        cv.put("description",description);
        cv.put("dtstart",dtstart);
        cv.put("dtend",dtend);
        cv.put("eventTimezone", TimeZone.getDefault().getID());
        context.getContentResolver().update(Events.CONTENT_URI,cv,"_id = '" + String.valueOf(_id) + "'",new String[0]);
    }

	public void delete(final Context context){
		final ContentResolver cr=context.getContentResolver();
		cr.delete(Uri.withAppendedPath(Events.CONTENT_URI, Long.toString(_id)), null, null);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeLong(_id);
		dest.writeString(organizer);
		dest.writeString(title);
		dest.writeString(eventLocation);
		dest.writeString(description);
		dest.writeInt(eventColour);
		dest.writeLong(dtstart);
		dest.writeLong(dtend);
		dest.writeLong(calendarID);
	}
}
