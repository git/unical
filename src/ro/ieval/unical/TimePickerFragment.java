package ro.ieval.unical;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.text.format.DateFormat;

public class TimePickerFragment extends DialogFragment {
	public static final String ARGUMENT_HOUR = "hour";
	public static final String ARGUMENT_MINUTE = "minute";

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Bundle args = getArguments();
		return new TimePickerDialog(getActivity(), (OnTimeSetListener) getActivity(),
									args.getInt(ARGUMENT_HOUR),
									args.getInt(ARGUMENT_MINUTE),
									DateFormat.is24HourFormat(getActivity()));
	}

}
