package ro.ieval.unical;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Fragment;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

public final class AddEventActivity extends Activity implements OnDateSetListener, OnTimeSetListener{

	private TextView dateTimeTextView;
    public static final String EXTRA_EVENT = "event";
    private static Event event=new Event();

    public static void editEvent(final Context c, final Event e) {
        final Intent i = new Intent(c,AddEventActivity.class);
        i.putExtra(AddEventActivity.EXTRA_EVENT,e);
        event=e;
        c.startActivity(i);
    }


	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.add_event_title);
		event = getIntent().hasExtra(EXTRA_EVENT) ? (Event) getIntent().getParcelableExtra(EXTRA_EVENT) : new Event();
		final ActionBar actionBar = getActionBar();
		setContentView(R.layout.add_event);

		final EditText title = (EditText) findViewById(R.id.eventtitle);
        title.requestFocus();
		title.setText(event.title);

		final TextView tstart = (TextView) findViewById(R.id.time_start);
		final TextView tend = (TextView) findViewById(R.id.time_end);
		final SimpleDateFormat sf=new SimpleDateFormat("HH:mm");
		final Date d=new Date(event.dtstart);
		tstart.setText(sf.format(d).toString());
		d.setTime(event.dtend);
		tend.setText(sf.format(d).toString());

		final SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		final TextView dstart = (TextView)findViewById(R.id.date_start);
		final TextView dend = (TextView)findViewById(R.id.date_end);
		d.setTime(event.dtstart);
		dstart.setText(sd.format(d).toString());
		d.setTime(event.dtend);
		dend.setText(sd.format(d).toString());
		final EditText description = (EditText) findViewById(R.id.description);
		description.setText(event.description);
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.add_event, menu);
		return true;
	}

    public void createCalendar() {
        Uri calUri,result;
        ContentValues cv;
        calUri = CalendarContract.Calendars.CONTENT_URI;
        cv = new ContentValues();
        cv.put(CalendarContract.Calendars.ACCOUNT_NAME,"unical");
        cv.put(CalendarContract.Calendars.ACCOUNT_TYPE,CalendarContract.ACCOUNT_TYPE_LOCAL);
        cv.put(CalendarContract.Calendars.NAME,"unical");
        cv.put(CalendarContract.Calendars.CALENDAR_DISPLAY_NAME, "Unical");
        cv.put(CalendarContract.Calendars.CALENDAR_COLOR, Color.BLACK);
        cv.put(CalendarContract.Calendars.CALENDAR_ACCESS_LEVEL, CalendarContract.Calendars.CAL_ACCESS_OWNER);
        cv.put(CalendarContract.Calendars.OWNER_ACCOUNT, "unical");
        cv.put(CalendarContract.Calendars.VISIBLE, 1);
        cv.put(CalendarContract.Calendars.SYNC_EVENTS, 1);
        cv.put(CalendarContract.Calendars.CALENDAR_TIME_ZONE, TimeZone.getDefault().getID());
        calUri = calUri.buildUpon()
                .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_NAME, "unical")
                .appendQueryParameter(CalendarContract.Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL)
                .build();

        this.getContentResolver().delete(calUri, CalendarContract.Calendars.ACCOUNT_NAME + " = 'unical'", new String[0]);
        this.getContentResolver().insert(calUri, cv);
        Event.calendarCreated=true;
    }

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.cancel:
				finish();
				return true;
            case R.id.save:
                boolean edit=false;
                if(event.title!=null) edit=true;
                event.title=((EditText)findViewById(R.id.eventtitle)).getText().toString();
                String start=((TextView)findViewById(R.id.date_start)).getText().toString()+ " " +((TextView)findViewById(R.id.time_start)).getText().toString();
                String end=((TextView)findViewById(R.id.date_end)).getText().toString()+ " "+((TextView)findViewById(R.id.time_end)).getText().toString();

                try {
                    Date s=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(start);
                    Date e=new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(end);
                    event.dtstart=s.getTime();
                    event.dtend=e.getTime();
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                event.description=((EditText) findViewById(R.id.description)).getText().toString();

                if(!Event.calendarCreated)createCalendar();
                if(event.calendarID!=Event.getUnicalId(this)) {
                    edit=false;
                    event.delete(this);
                }
                event.calendarID=Event.getUnicalId(this);
                if(!edit) event.insert(this);
                else event.edit(this);

                Toast toast = Toast.makeText(getApplicationContext(),"Event saved!",Toast.LENGTH_LONG);
                toast.show();
                finish();
                return true;
			default:
				return false;
		}
	}

    public void showDateTimePickerDialog(final View v) {
		dateTimeTextView=(TextView) v;
		if(v.getId()==R.id.date_start || v.getId()==R.id.date_end ){
			final String[] parts = dateTimeTextView.getText().toString().split("-");
			final DatePickerFragment fragment = new DatePickerFragment();
			final Bundle args = new Bundle();
			args.putInt(DatePickerFragment.ARGUMENT_YEAR, Integer.parseInt(parts[0]));
			args.putInt(DatePickerFragment.ARGUMENT_MONTH, Integer.parseInt(parts[1]));
			args.putInt(DatePickerFragment.ARGUMENT_DAY, Integer.parseInt(parts[2]));
			fragment.setArguments(args);
			fragment.show(getFragmentManager(), "datePicker");
		} else {
			final String[] parts = dateTimeTextView.getText().toString().split(":");
			final TimePickerFragment fragment = new TimePickerFragment();
			final Bundle args = new Bundle();
			args.putInt(TimePickerFragment.ARGUMENT_HOUR, Integer.parseInt(parts[0]));
			args.putInt(TimePickerFragment.ARGUMENT_MINUTE, Integer.parseInt(parts[1]));
			fragment.setArguments(args);
			fragment.show(getFragmentManager(), "timePicker");
		}
	}

	@Override
	public void onTimeSet(final TimePicker picker, final int hour, final int minute) {
		dateTimeTextView.setText(String.format("%02d:%02d",hour,minute));
		if(dateTimeTextView.getId() == R.id.time_start)
			((TextView)findViewById(R.id.time_end)).setText(String.format("%02d:%02d",hour,minute));
	}

	@Override
	public void onDateSet(final DatePicker picker, final int year, final int monthOfYear, final int dayOfMonth) {
		final Calendar calendar=new GregorianCalendar(year, monthOfYear, dayOfMonth);
		dateTimeTextView.setText(Utils.formatDate(year, monthOfYear, dayOfMonth));
		//calendar.add(Calendar.DAY_OF_MONTH, 1);
		if(dateTimeTextView.getId() == R.id.date_start)
			((TextView)findViewById(R.id.date_end)).setText(Utils.formatDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
	}
}
