package ro.ieval.unical;

import android.database.Cursor;

public final class Application extends android.app.Application {
	public AlarmOpenHelper alarmOpenHelper;

	@Override
	public void onCreate(){
		alarmOpenHelper = new AlarmOpenHelper(this);
		final Cursor cur = alarmOpenHelper.getReadableDatabase().query(AlarmOpenHelper.TABLE, null, null, null, null, null, null);
		if(cur.moveToFirst()) {
			do {
				if(cur.getLong(0) > System.currentTimeMillis())
					Utils.setAlarm(this, cur.getLong(0), cur.getLong(1));
				cur.moveToNext();
			} while(!cur.isAfterLast());
		}
	}
}
