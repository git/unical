package ro.ieval.unical;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

public final class EventListActivity extends ListActivity {
	private static class EventComparator implements Comparator<Event> {
		@Override
		public int compare(final Event lhs, final Event rhs) {
			if(lhs.dtstart < rhs.dtstart)
				return -1;
			if(lhs.dtstart == rhs.dtstart)
				return 0;
			return 1;
		}

	}

	private class EventAdapter extends ArrayAdapter<Event> {
		public EventAdapter() {//NOPMD
			super(EventListActivity.this, R.layout.event_row_layout, events);
		}

		@Override
			public View getView(final int position, final View convertView, final ViewGroup parent) {
			final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final View rowView = inflater.inflate(R.layout.event_row_layout, parent, false);
			final TextView title = (TextView) rowView.findViewById(R.id.smalleventtitle);
			final TextView dstart = (TextView) rowView.findViewById(R.id.smalldatestart);
			final TextView dend = (TextView) rowView.findViewById(R.id.smalldateend);
			final View header = rowView.findViewById(R.id.rowHeader);
			final DateFormat format = DateFormat.getDateTimeInstance();
			header.setBackgroundColor(events[position].eventColour);
			title.setText(events[position].title);
			dstart.setText(format.format(new Date(events[position].dtstart)));
			dend.setText(format.format(new Date(events[position].dtend)));

			return rowView;
		}
	}

	private Event[] events;

	@Override
	public void onListItemClick(final ListView view, final View v, final int pos, final long id) {
		DisplayEventActivity.displayEvent(this, events[pos]._id);
	}

	private void displayEvents() {
		events = Event.getAllEvents(this);

		Arrays.sort(events, new EventComparator());
		int idx=-1;//index of the first event to be displayed
		for(int i=0; i<events.length; ++i)
			if(idx==-1 && events[i].dtend>new Date().getTime()) idx=i;
		if(idx==-1) idx=0;

		setListAdapter(new EventAdapter());
		getListView().setSelectionFromTop(idx, 0);
	}

    @Override
    protected  void onStart() {
        super.onStart();
        displayEvents();
    }

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		displayEvents();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.event_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
			case R.id.add_event:
				startActivity(new Intent(this,AddEventActivity.class));
				return true;
			case R.id.refresh:
				displayEvents();
				return true;
			default:
				return false;
		}
	}

}
