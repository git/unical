package ro.ieval.unical;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.CalendarContract.Calendars;

final class Calendar {
	private static final String[] PROJECTION = {
		BaseColumns._ID,
		Calendars.NAME,
		Calendars.CALENDAR_DISPLAY_NAME,
		Calendars.VISIBLE,
		Calendars.ACCOUNT_NAME,
		Calendars.ACCOUNT_TYPE,
		Calendars.CALENDAR_COLOR,
	};

	/** Calendar ID */
	public final long _id;
	/** Calendar name */
	public final String name;
	/** Calendar display name */
	public final String displayName;
	/** Calendar visibility (false - do not show events associated with this calendar, true - show events associated with this calendar) */
	public final boolean visible;
	/** Name of the account used to sync this calendar */
	public final String accountName;
	/** Type of the calendar used to sync this calendar */
	public final String accountType;
	/** Calendar colour */
	public final int colour;

	private Calendar(final long _id, final String name, final String displayName, final boolean visible, final String accountName, final String accountType, final int colour) {
		this._id=_id;
		this.name=name;
		this.displayName=displayName;
		this.visible=visible;
		this.accountName=accountName;
		this.accountType=accountType;
		this.colour=colour;
	}

	public static Calendar[] getAllCalendars(final Context context){
		final ContentResolver cr=context.getContentResolver();
		final Cursor cursor = cr.query(Calendars.CONTENT_URI, PROJECTION, null, null, null);
		cursor.moveToFirst();
		final Calendar[] calendars = new Calendar[cursor.getCount()];
		for(int i=0;i<calendars.length;i++){
			calendars[i]=new Calendar(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)!=0, cursor.getString(4), cursor.getString(5), cursor.getInt(6));
			cursor.moveToNext();
		}
		cursor.close();
		return calendars;
	}

	public static Calendar getCalendarById(final Context context, final long id){
		final ContentResolver cr=context.getContentResolver();
		final Cursor cursor = cr.query(Calendars.CONTENT_URI, PROJECTION, BaseColumns._ID + " = ?", new String[]{Long.toString(id)}, null);
		final Calendar calendar;
		if(cursor.moveToFirst())
			calendar = new Calendar(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3)!=0, cursor.getString(4), cursor.getString(5), cursor.getInt(6));
		else
			calendar = null;
		cursor.close();
		return calendar;
	}
}
