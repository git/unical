package ro.ieval.unical;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.*;
import java.util.Calendar;

public class AddAlarmActivity  extends ListActivity {
	public static final String EXTRA_EVENT = "event";
    private static Event event;

    public static void addAlarm(final Context c, final Event e) {
        final Intent i = new Intent(c,AddAlarmActivity.class);
        i.putExtra(AddAlarmActivity.EXTRA_EVENT,e);
        event=e;
        c.startActivity(i);
    }


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_alarm);
        setTitle(R.string.add_alarm);
        event = getIntent().hasExtra(EXTRA_EVENT) ? (Event) getIntent().getParcelableExtra(EXTRA_EVENT) : new Event();
        final DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
        Date d=new Date(Math.max(event.dtstart,GregorianCalendar.getInstance().getTimeInMillis()));
        GregorianCalendar gc=new GregorianCalendar();
        gc.setTime(d);
        datePicker.init(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DAY_OF_MONTH),new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            }
        });
        timePicker.setCurrentHour(gc.get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(gc.get(Calendar.MINUTE));

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_alarm, menu);
        return true;
    }

	@Override
	public boolean onOptionsItemSelected(final MenuItem item){
		switch(item.getItemId()){
		case R.id.save:
			final DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
			final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);
			final GregorianCalendar calendar = new GregorianCalendar(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getCurrentHour(), timePicker.getCurrentMinute(), 0);
			try {
                Utils.addAlarm((Application) getApplication(), calendar.getTimeInMillis(), event._id);
                finish();
            }catch(SQLException e)  {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.alarm_error)
                        .setTitle(R.string.alarm_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();
            }

			return true;
		default:
			return false;
		}
	}
}
