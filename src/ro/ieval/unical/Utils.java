package ro.ieval.unical;

import java.util.GregorianCalendar;
import java.util.Locale;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.view.ViewGroup;

final class Utils {
	private Utils(){ /* do nothing */ }

	public static void setEnabledRecursively(final View view, final boolean enabled){
		view.setEnabled(enabled);
		if(view instanceof ViewGroup){
			final ViewGroup group=(ViewGroup) view;
			for(int i=0;i<group.getChildCount();i++)
				setEnabledRecursively(group.getChildAt(i), enabled);
		}
	}

	public static String formatDate(final int year, final int month, final int day){
		return String.format(Locale.ENGLISH, "%4d-%02d-%02d", year, month+1, day);
	}

	public static void addAlarm(final Application application, final long time, final long eventId) throws SQLException{
		final SQLiteDatabase db = application.alarmOpenHelper.getWritableDatabase();
		final ContentValues values = new ContentValues(2);
		values.put(AlarmOpenHelper.TIME, time);
		values.put(AlarmOpenHelper.EVENT, eventId);
		db.insertOrThrow(AlarmOpenHelper.TABLE, null, values);
		setAlarm(application, time, eventId);
	}

	public static void deleteAlarm(final Application application, final long time) throws SQLException{
		final SQLiteDatabase db = application.alarmOpenHelper.getWritableDatabase();
		db.delete(AlarmOpenHelper.TABLE, AlarmOpenHelper.TIME + " = " + time, new String[0]);
		unsetAlarm(application, time);
	}

	public static Long[] getAlarmsByEvent(final Application application, final long eventId) throws SQLException{
		final SQLiteDatabase db = application.alarmOpenHelper.getWritableDatabase();
		db.delete(AlarmOpenHelper.TABLE, AlarmOpenHelper.TIME + " < " + System.currentTimeMillis(), new String[0]);
		final Cursor cursor = db.query(AlarmOpenHelper.TABLE, new String[]{AlarmOpenHelper.TIME}, AlarmOpenHelper.EVENT + " = " + eventId, new String[0], null, null, null);
		if(cursor.moveToFirst()){
			final Long[] ret = new Long[cursor.getCount()];
			for(int i=0;i<ret.length;i++){
				ret[i]=cursor.getLong(0);
				cursor.moveToNext();
			}
			return ret;
		}
		return new Long[0];
	}

	public static void setAlarm(final Context context, final long time, final long eventId){
		final Intent intent = new Intent(context, AlarmReceiverActivity.class);
		intent.setAction(Long.toString(time));
		intent.putExtra(AlarmReceiverActivity.EXTRA_EVENT, eventId);
		final AlarmManager man = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		man.set(AlarmManager.RTC_WAKEUP, time, PendingIntent.getActivity(context, 0, intent, 0));
	}

	public static void unsetAlarm(final Context context, final long time){
		final Intent intent = new Intent(context, AlarmReceiverActivity.class);
		intent.setAction(Long.toString(time));
		final AlarmManager man = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		man.cancel(PendingIntent.getActivity(context, 0, intent, 0));
	}
}
