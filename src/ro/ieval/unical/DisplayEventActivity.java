package ro.ieval.unical;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

public class DisplayEventActivity extends Activity {
	private final class DeleteListener implements View.OnClickListener {
		private final long time;
		DeleteListener(final long time){
			this.time=time;
		}

		@Override public void onClick(final View v){
			Utils.deleteAlarm((Application) getApplication(), time);
			setAdapter();
		}
	}

	public static final String EXTRA_EVENT = "event";
	private Event event;
    private final DeleteEventDialog d=new DeleteEventDialog();

	private void setAdapter(){
		final ListView alarms = (ListView) findViewById(R.id.alarm_list);
		alarms.setAdapter(new ArrayAdapter<Long>(this, R.layout.alarm_row_layout, Utils.getAlarmsByEvent((Application) getApplication(), event._id)){
				@Override public View getView(final int position, final View convertView, final ViewGroup parent) {
					final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					final View rowView = inflater.inflate(R.layout.alarm_row_layout, parent, false);
					final TextView date = (TextView) rowView.findViewById(R.id.alarm_datetime);
					final ImageButton delete = (ImageButton) rowView.findViewById(R.id.alarm_delete);
					delete.setOnClickListener(new DeleteListener(getItem(position)));
					date.setText(DateFormat.getDateTimeInstance().format(getItem(position)));

					return rowView;
				}
			});
	}

	public static void displayEvent(final Context context, final long id){
		final Intent i=new Intent(context,DisplayEventActivity.class);
		i.putExtra(DisplayEventActivity.EXTRA_EVENT, id);
		context.startActivity(i);
	}

	@Override
	protected void onStart() {
		super.onStart();
		event=getIntent().hasExtra(EXTRA_EVENT) ? Event.getEventById(this, getIntent().getLongExtra(EXTRA_EVENT, 0)) : new Event();
		setContentView(R.layout.event_view);
		setTitle(event.title);

		final TextView startDay= (TextView) findViewById(R.id.startday);
		final TextView endDay= (TextView) findViewById(R.id.endday);
		final TextView startDate= (TextView) findViewById(R.id.startdate);
		final TextView endDate= (TextView) findViewById(R.id.enddate);
		final TextView startTime= (TextView) findViewById(R.id.starttime);
		final TextView endTime= (TextView) findViewById(R.id.endtime);
		final TextView description= (TextView) findViewById(R.id.event_view_description);;

		final Date start=new Date(event.dtstart);
		final Date end=new Date(event.dtend);
		final DateFormat dateFormat = DateFormat.getDateInstance();
		final DateFormat timeFormat = DateFormat.getTimeInstance();
		final DateFormat dayOfWeekFormat = new SimpleDateFormat("EEEEEEE", Locale.UK);

		startDay.setText(dayOfWeekFormat.format(start));
		startDate.setText(dateFormat.format(start));
		if(!dateFormat.format(start).equals(dateFormat.format(end))) {
			endDay.setText(dayOfWeekFormat.format(end));
			endDate.setText(dateFormat.format(end));
		}

		startTime.setText(timeFormat.format(start));
		if(!start.equals(end))
			endTime.setText(timeFormat.format(end));

		description.setText(event.description);
		setAdapter();
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.event, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle item selection
		switch (item. getItemId()) {
		case R.id.deleteevent:
            final Bundle b=new Bundle();
            b.putParcelable("Event",event);
            d.setArguments(b);
			d.show(getFragmentManager(), "Delete");

			return true;
		case R.id.edit_event:
			AddEventActivity.editEvent(this, event);
			return true;
		case R.id.add_alarm:
			AddAlarmActivity.addAlarm(this,event);
			return true;
		default:
			return false;
		}
	}
}
