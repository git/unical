package ro.ieval.unical;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class AlarmOpenHelper extends SQLiteOpenHelper{
	public static final String TIME = "time";
	public static final String EVENT = "event";
	public static final String TABLE = "alarms";

	private static final int VERSION = 1;
	private static final String DB = "alarms";

	AlarmOpenHelper (final Context context) {
		super(context, DB, null, VERSION);
	}

	@Override
	public void onCreate(final SQLiteDatabase db){
		db.execSQL("CREATE TABLE " + TABLE + " (" +
				   TIME + " INTEGER PRIMARY KEY, " +
				   EVENT + " INTEGER NOT NULL);");
	}

	@Override
	public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion){
		// do nothing
	}
}
