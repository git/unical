package ro.ieval.unical;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class AlarmReceiverActivity extends Activity {
	public static final String EXTRA_EVENT = "event";
	private MediaPlayer mMediaPlayer;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Event event=Event.getEventById(this,getIntent().getLongExtra(EXTRA_EVENT,1L));
		setContentView(R.layout.alarm);

        TextView title = (TextView)findViewById(R.id.alarm_title);
        title.setText(event.title);

        final TextView startDay= (TextView) findViewById(R.id.startday);
        final TextView endDay= (TextView) findViewById(R.id.endday);
        final TextView startDate= (TextView) findViewById(R.id.startdate);
        final TextView endDate= (TextView) findViewById(R.id.enddate);
        final TextView startTime= (TextView) findViewById(R.id.starttime);
        final TextView endTime= (TextView) findViewById(R.id.endtime);
        final Date start=new Date(event.dtstart);
        final Date end=new Date(event.dtend);
        final DateFormat dateFormat = DateFormat.getDateInstance();
        final DateFormat timeFormat = DateFormat.getTimeInstance();
        final DateFormat dayOfWeekFormat = new SimpleDateFormat("EEEEEEE", Locale.UK);

        startDay.setText(dayOfWeekFormat.format(start));
        startDate.setText(dateFormat.format(start));
        if(!dateFormat.format(start).equals(dateFormat.format(end))) {
            endDay.setText(dayOfWeekFormat.format(end));
            endDate.setText(dateFormat.format(end));
        }

        startTime.setText(timeFormat.format(start));
        if(!start.equals(end))
            endTime.setText(timeFormat.format(end));
		
		final Button stopAlarm = (Button) findViewById(R.id.stopAlarm);
        stopAlarm.setOnTouchListener(new OnTouchListener() {
            @Override
			public boolean onTouch(final View arg0, final MotionEvent arg1) {
                mMediaPlayer.stop();
                finish();
                return false;
            }
        });
 
        playSound(this, getAlarmUri());
    }
 
    private void playSound(final Context context, final Uri alert) {
        mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
        } catch (IOException e) {
        	e.printStackTrace();
        }
    }

    private static Uri getAlarmUri() {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null)
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (alert == null) 
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
           
        return alert;
    }
}
