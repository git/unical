package ro.ieval.unical;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

public class DatePickerFragment extends DialogFragment {
	public static final String ARGUMENT_YEAR = "year";
	public static final String ARGUMENT_MONTH = "month";
	public static final String ARGUMENT_DAY = "day";

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Bundle args = getArguments();
		return new DatePickerDialog(getActivity(), (OnDateSetListener) getActivity(),
									args.getInt(ARGUMENT_YEAR),
									args.getInt(ARGUMENT_MONTH),
									args.getInt(ARGUMENT_DAY));
	}
}
