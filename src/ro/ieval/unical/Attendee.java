package ro.ieval.unical;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.CalendarContract.Attendees;

final class Attendee {
	private static final String[] PROJECTION = {
		BaseColumns._ID,
		Attendees.EVENT_ID,
		Attendees.ATTENDEE_NAME,
		Attendees.ATTENDEE_EMAIL,
		Attendees.ATTENDEE_RELATIONSHIP,
		Attendees.ATTENDEE_TYPE,
		Attendees.ATTENDEE_STATUS,
	};

	/** Attendee ID */
	public final long _id;
	/** Event ID */
	public final long eventID;
	/** Attendee name */
	public final String name;
	/** Attendee email address **/
	public final String email;
	/**
	 * Attendee relationship.
	 *
	 * One of {@link CalendarContract.AttendeesColumns#RELATIONSHIP_ATTENDEE RELATIONSHIP_ATENDEE},
	 * {@link CalendarContract.AttendeesColumns#RELATIONSHIP_NONE RELATIONSHIP_NONE},
	 * {@link CalendarContract.AttendeesColumns#RELATIONSHIP_ORGANIZER RELATIONSHIP_ORGANIZER},
	 * {@link CalendarContract.AttendeesColumns#RELATIONSHIP_PERFORMER RELATIONSHIP_PERFORMER},
	 * {@link CalendarContract.AttendeesColumns#RELATIONSHIP_SPEAKER RELATIONSHIP_SPEAKER}
	 */
	public final int relationship;
	/**
	 * Attendee type.
	 *
	 * One of {@link CalendarContract.AttendeesColumns#TYPE_NONE TYPE_NONE},
	 * {@link CalendarContract.AttendeesColumns#TYPE_OPTIONAL TYPE_OPTIONAL},
	 * {@link CalendarContract.AttendeesColumns#TYPE_REQUIRED TYPE_REQUIRED},
	 * {@link CalendarContract.AttendeesColumns#TYPE_RESOURCE TYPE_RESOURCE}
	 */
	public final int type;
	/**
	 * Attendee status.
	 *
	 * One of {@link CalendarContract.AttendeesColumns#ATTENDEE_STATUS_ACCEPTED ATTENDEE_STATUS_ACCEPTED},
	 * {@link CalendarContract.AttendeesColumns#ATTENDEE_STATUS_DECLINED ATTENDEE_STATUS_DECLINED},
	 * {@link CalendarContract.AttendeesColumns#ATTENDEE_STATUS_INVITED ATTENDEE_STATUS_INVITED},
	 * {@link CalendarContract.AttendeesColumns#ATTENDEE_STATUS_NONE ATTENDEE_STATUS_NONE},
	 * {@link CalendarContract.AttendeesColumns#ATTENDEE_STATUS_TENTATIVE ATTENDEE_STATUS_TENTATIVE}
	 */
	public final int status;

	private Attendee(final long _id, final long eventID, final String name, final String email, final int relationship, final int type, final int status){
		this._id=_id;
		this.eventID=eventID;
		this.name=name;
		this.email=email;
		this.relationship=relationship;
		this.type=type;
		this.status=status;
	}

	public static Attendee[] getAttendeesByEvent(final Context context, final long eventID){
		final ContentResolver cr=context.getContentResolver();
		final Cursor cursor = Attendees.query(cr, eventID, PROJECTION);
		cursor.moveToFirst();
		final Attendee[] attendees = new Attendee[cursor.getCount()];
		for(int i=0;i<attendees.length;i++){
			attendees[i]=new Attendee(cursor.getLong(0), cursor.getLong(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6));
			cursor.moveToNext();
		}
		cursor.close();
		return attendees;
	}
}
