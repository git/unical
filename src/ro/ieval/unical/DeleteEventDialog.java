package ro.ieval.unical;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

@SuppressLint("ValidFragment")
public final class DeleteEventDialog extends DialogFragment {
	private Event event;

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        event=getArguments().getParcelable("Event");
		builder.setMessage(R.string.askdeleteevent)
			   .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				   @Override
				   public void onClick(final DialogInterface dialog, final int id) {
					   event.delete(getActivity());
					   getActivity().finish();
				   }
			   })
			   .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
				   @Override
				   public void onClick(final DialogInterface dialog, final int id) {
					   // User cancelled the dialog
				   }
			   });
		return builder.create();
	}
}
